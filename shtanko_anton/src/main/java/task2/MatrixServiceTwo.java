package task2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class MatrixServiceTwo implements ServiceTwo {

    @Override
    public int sum(int[][] matrix, int nthreads) {
        int result = 0;
        if (nthreads > matrix.length) {
            nthreads = matrix.length;
        }
        int count = 0;
        ExecutorService service = Executors.newFixedThreadPool(nthreads);
        List<Future<Integer>> futureList = new ArrayList<>();

        while (count != matrix.length) {
            futureList.add(service.submit(new ColumnSummator(matrix[count])));
            count++;
        }
        for (Future<Integer> future : futureList) {
            try {
                result += future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        service.shutdown();

        return result;
    }

    private final class ColumnSummator implements Callable<Integer> {
        private int totalSum;
        private final int[] matrix;


        public ColumnSummator(int[] matrix) {
            this.matrix = matrix;
        }

        public int getTotalSum() {
            return totalSum;
        }

        public void setTotalSum(int totalSum) {
            this.totalSum = totalSum;
        }

        @Override
        public Integer call() throws Exception {
            for (int number : matrix) {
                setTotalSum(getTotalSum() + number);
            }
            return totalSum;
        }
    }
}
