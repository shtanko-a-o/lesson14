package task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MatrixServiceOne implements ServiceOne {

    private final List<int[]> listRow = new ArrayList<>();

    public int sum(int[][] matrix, int nthreads) {
        int result = 0;
        if (nthreads > matrix.length) {
            nthreads = matrix.length;
        }

        listRow.addAll(Arrays.asList(matrix));

        double temp = Math.round((double) listRow.size() / nthreads);
        int lengthTempList = (int) temp;

        List<int[]> tempList = new ArrayList<>();
        for (int i = 0; i < nthreads; i++) {
            for (int j = 0; j < lengthTempList; j++) {
                if (!listRow.isEmpty()) {
                    tempList.add(listRow.get(0));
                    listRow.remove(0);
                    if (i == nthreads - 1 && !listRow.isEmpty()) {
                        while (!listRow.isEmpty()) {
                            tempList.add(listRow.get(0));
                            listRow.remove(0);
                        }
                    }
                }
            }
            RowSummator rowSummator = new RowSummator(tempList);
            Thread thread = new Thread(rowSummator);
            thread.start();
            synchronized (thread) {
                try {
                    thread.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            result += rowSummator.getTotalSum();
            tempList.clear();
        }
        return result;
    }

    private final class RowSummator implements Runnable {
        private int totalSum;
        private final List<int[]> tempList;

        public RowSummator(List<int[]> tempList) {
            this.tempList = tempList;
        }

        public int getTotalSum() {
            return totalSum;
        }

        public void setTotalSum(int totalSum) {
            this.totalSum = totalSum;
        }

        public void run() {
            synchronized (this) {
                for (int i = 0; i < tempList.size(); i++) {
                    int[] row = tempList.get(i);
                    for (int j = 0; j < row.length; j++) {
                        setTotalSum(getTotalSum() + row[j]);
                    }
                }
                this.notify();
            }
        }
    }
}

