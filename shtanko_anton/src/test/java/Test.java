import task1.MatrixServiceOne;
import task2.MatrixServiceTwo;

public class Test {
    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3, 4, 5},   //ответ 80
                {1, 2, 3, 4, 5},
                {5, 5, 5, 5, 5},
                {5, 5, 5, 5, 5},
        };

        System.out.println(new MatrixServiceOne().sum(matrix, 2));

        System.out.println(new MatrixServiceTwo().sum(matrix, 2));
    }
}
